import { ObservablesStatePage } from './app.po';

describe('observables-state App', () => {
  let page: ObservablesStatePage;

  beforeEach(() => {
    page = new ObservablesStatePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
