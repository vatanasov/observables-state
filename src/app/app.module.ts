import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OneComponentComponent } from './one-component/one-component.component';
import { TwoComponentComponent } from './two-component/two-component.component';
import { MasterComponentComponent } from './master-component/master-component.component';
import {RouterModule} from "@angular/router";
import {ModelService} from "./model.service";

@NgModule({
  declarations: [
    AppComponent,
    OneComponentComponent,
    TwoComponentComponent,
    MasterComponentComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([{
      path: "",
      component: OneComponentComponent
    },{
      path: "one",
      component: OneComponentComponent
    },{
      path: "two",
      component: TwoComponentComponent
    }])
  ],
  providers: [ModelService],
  bootstrap: [AppComponent]
})
export class AppModule { }
