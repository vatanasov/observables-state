import { Component, OnInit } from '@angular/core';
import {ModelService} from "../model.service";
import {MasterModel} from "../models/master.model";

@Component({
  selector: 'app-one-component',
  templateUrl: './one-component.component.html',
  styleUrls: ['./one-component.component.css']
})
export class OneComponentComponent implements OnInit {
  public model: MasterModel;
  constructor(
    private _modelService: ModelService) {
    this.model = _modelService.masterModel;
  }

  ngOnInit() {
    console.log(this.model.modelTwoState.isDirty());
  }

}
