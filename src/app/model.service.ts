import { Injectable } from '@angular/core';
import {MasterModel} from "./models/master.model";
import {OneModel} from "./models/one.model";
import {TwoModel} from "./models/two.model";

@Injectable()
export class ModelService {
  private _masterModel: MasterModel;
  constructor() {
    console.log('ModelService::constrctor');
    this._masterModel =
      new MasterModel([new OneModel(), new TwoModel()]);
  }

  public get masterModel(): MasterModel {
    return this._masterModel;
  }

}
