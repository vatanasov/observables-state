import {Model} from "./model.interface";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
/**
 * Created by vasil on 09.07.17.
 */
export class OneModel implements Model{
  public id: string = "one-model";
  public value: string = "";

  public copy(spec?: {[key: string]: any}): OneModel {
    let o = new OneModel();
    o.id = this.id;
    o.value =this.value;
    if (spec) {
      for (let key in spec) {
        o[key] = spec[key];
      }
    }
    return o;
  }

}
