import {BehaviorSubject} from "rxjs/BehaviorSubject";

export class MemorySubjectModel<T> extends BehaviorSubject<T> {

  private _dirty: boolean = false;

  private _memo: T;

  constructor(value: T) {
    super(value);
    this._memo = this.copy(value);
    this.subscribe(() => {
      if (!this._dirty) {
        this._dirty = !this.equals(this._memo, this.copy(this.getValue()));
      }
    });
  }

  public getMemo(): T {
    return this._memo;
  }

  public isDirty(): boolean {
    return this._dirty;
  }

  public clean(): void {
    this._memo = this.copy(this.getValue());
    this._dirty = false;
  }

  public reset(): void {
    this._memo = null;
    this._dirty = false;
  }

  public changeValue(valueChanges: {[key: string]: any} | any): void {
    let value: T = this.getValue();

    if (typeof value !== "object") {
      return this.next(valueChanges);
    }

    for (let key in valueChanges) {
      value[key] = valueChanges[key];
    }

    return this.next(value);
  }

  protected equals(one: Object, two: Object): boolean {
    if (one === two) {
      return true;
    }

    if (typeof one !== "object" || typeof two !== "object") {
      return false;
    }

    if (Object.keys(one).length !== Object.keys(two).length) {
      return false;
    }

    for (let key in one) {
      if (one[key] !== two[key]) {
        return false;
      }
    }

    return true;
  }

  private copy(object: any): any {

    if (typeof object !== "object") {
      return object;
    }

    let o: Object = {};
    for (let key in object) {
      if (object.hasOwnProperty(key)) {
        o[key] = object[key];
      }
    }

    return o;
  }

}
