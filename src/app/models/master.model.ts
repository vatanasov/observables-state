import {Model} from "./model.interface";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {MemorySubjectModel} from "./memory-subject.model";

/**
 * Created by vasil on 09.07.17.
 */

export class MasterModel {

  public get modelOneState(): MemorySubjectModel<any> {
    return this.getState("one-model");
  }

  public get modelTwoState():MemorySubjectModel<any> {
    return this.getState("two-model");
  }

  private _states: {[key: string]:  MemorySubjectModel<any>};

  constructor(models: Model[]) {
    this._states = {};
    for (let model of models) {
      this.setState(model.id, new MemorySubjectModel(model));
    }

    this.modelOneState.subscribe((value: any) => {
      console.log(this.modelOneState.isDirty(), this.modelOneState.getValue());
    });


    this.modelTwoState.subscribe((value: any) => {
      console.log(this.modelTwoState.isDirty(), this.modelTwoState.getValue());
    });
  }

  public getState(modelId: string): MemorySubjectModel<any> {
    return this._states[modelId];
  }


  private setState(modelId: string, data:  MemorySubjectModel<any>): void {
    this._states[modelId] = data;
  }
}
