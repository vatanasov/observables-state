import {Model} from "./model.interface";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
/**
 * Created by vasil on 09.07.17.
 */

export class TwoModel implements Model{
  public id: string = "two-model";
  public value: string = "";
  public copy(spec?: {[key: string]: any}): TwoModel {
    let o = new TwoModel();
    o.id = this.id;
    o.value =this.value;
    if (spec) {
      for (let key in spec) {
        o[key] = spec[key];
      }
    }

    return o;
  }

  public copyShalow(spec?: {[key: string]: any}): TwoModel {
    let o = this;
    if (spec) {
      for (let key in spec) {
        o[key] = spec[key];
      }
    }

    return o;
  }
}
