import {BehaviorSubject} from "rxjs/BehaviorSubject";
/**
 * Created by vasil on 09.07.17.
 */
export interface Model {
  id: any;
  value: any;
  copy(): Model;
}
