import { Component, OnInit } from '@angular/core';
import {ModelService} from "../model.service";
import {MasterModel} from "../models/master.model";
import {MemorySubjectModel} from "../models/memory-subject.model";
import {Model} from "../models/model.interface";

@Component({
  selector: 'app-two-component',
  templateUrl: './two-component.component.html',
  styleUrls: ['./two-component.component.css']
})
export class TwoComponentComponent implements OnInit {

  public model: MasterModel;
  public state: MemorySubjectModel<Model>;
  constructor(private _modelService: ModelService) {
    this.state = _modelService.masterModel.modelTwoState;
    this.model = _modelService.masterModel;
  }


  ngOnInit() {
    console.log(this.model.modelTwoState.isDirty());
    this.state.clean();
  }

}
